<?php

/**
 * @namespace
 */
namespace Application;

use Bluz\Proxy\Layout;

return
/**
 * @privilege Administration
 *
 * @return void
 */
function () {
    /**
     * @var Bootstrap $this
     */
    Layout::breadCrumbs(
        [
            __('Administration'),
        ]
    );
    Layout::setTemplate('administration.phtml');
};
